package practice.android.howtodraw

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View
import android.graphics.Path
import android.view.MotionEvent

class DrawableCanvas(context:Context, attrs: AttributeSet):View(context, attrs) {

    fun clear() {

    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return super.onTouchEvent(event)
    }


    fun convert(a:List<*>): List<Int>? {
        val aTemp = a as? List<Int> ?: return null
        return a
    }

    fun getViaPoints(list: List<*>): List<Int>? {

        list.forEach { if(it !is Int ) return null }

        val waypointList = list as List<Int>

       // val waypointList = list.filterIsInstance<Int>()

        return waypointList.filter{ waypointList.indexOf(it) != 0 && waypointList.indexOf(it) != waypointList.lastIndex}
    }

}


