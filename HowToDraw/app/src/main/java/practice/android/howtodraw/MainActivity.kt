package practice.android.howtodraw

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setClearButtonAction()
        setRadioGroupAction()
        setSeekBarAction()
    }

    private fun setClearButtonAction() {
        clearButton.setOnClickListener {
            drawableCanvas.clear()
        }
    }

    private fun setRadioGroupAction() {
        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            didCheckRadioButtonWith(checkedId)
        }
    }

    private fun setSeekBarAction() {
        seekBar.setOnSeekBarChangeListener(object:SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
    }

    private fun didCheckRadioButtonWith(id:Int) {
        var color: Int
        when(id) {
            R.id.blackRadioButton -> color = Color.BLACK
            R.id.redRadioButton -> color = Color.RED
            R.id.greenRadioButton -> color = Color.GREEN
            R.id.blueRadioButton -> color = Color.BLUE
        }
        
    }
}
