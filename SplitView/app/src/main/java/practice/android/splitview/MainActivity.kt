package practice.android.splitview

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.widget.Toast

import practice.android.splitview.RecyclerViewAdapters.ListRecyclerViewAdapter

class MainActivity : AppCompatActivity(), ListRecyclerViewAdapter.OnItemSelectionListener {

    private val receiver = object:BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            recieve(intent)
        }
    }

    private fun recieve(intent:Intent?) {
        val value = intent?.getStringExtra(Constants.DATA_KEY) ?: "null"
        Toast.makeText(this, value, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val items = mutableListOf<String>()
        for (i in 1..100) {
            items.add(i.toString())
        }

        val fragment = fragmentManager.findFragmentById(R.id.listFragment) as ListFragment
        fragment.setItems(items)

        registerBroadCast()
    }

    private fun registerBroadCast() {
        val notName = IntentFilter(Constants.NOTIFICATION_NAME)
        val manager = LocalBroadcastManager.getInstance(this)
        manager.registerReceiver(receiver, notName)
    }

    private fun deRegisterBroadCast() {
        val manager = LocalBroadcastManager.getInstance(this)
        manager.unregisterReceiver(receiver)
    }

    override fun onDestroy() {
        super.onDestroy()
        deRegisterBroadCast()
    }

    override fun onItemSelected(value: String?) {
        if (value == null) {
            return
        }
        val detailFragment = fragmentManager.findFragmentById(R.id.detailFragment) as? DetailFragment
        if(detailFragment == null || !detailFragment.isInLayout) {
            val intent = Intent(this,  DetailActivity::class.java)
            intent.putExtra(DetailActivity.VALUE, value)
            startActivity(intent)
        } else {
            detailFragment?.setDetailValue(value)
        }
    }
}

