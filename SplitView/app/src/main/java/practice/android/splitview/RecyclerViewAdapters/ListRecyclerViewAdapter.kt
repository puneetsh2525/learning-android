package practice.android.splitview.RecyclerViewAdapters

import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import practice.android.splitview.Constants

class ListRecyclerViewAdapter constructor (private val items:List<String>): RecyclerView.Adapter<ListRecyclerViewAdapter.ListViewHolder>() {

    interface OnItemSelectionListener {
        fun onItemSelected(value: String?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
        val viewHolder = ListViewHolder(view)
        viewHolder.listener = parent.context as? OnItemSelectionListener
        viewHolder.context = parent.context
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(items[position])
    }

    class ListViewHolder(v:View):RecyclerView.ViewHolder(v), View.OnClickListener {

        private var item:String? = null
        private val textView:TextView
        public var listener: OnItemSelectionListener? = null
        public var context:Context? = null

        init {
            v.setOnClickListener(this)
            textView = v.findViewById(android.R.id.text1)
        }

        fun bind(value:String) {
            this.item = value
            textView.text = value
        }

        override fun onClick(v: View?) {
            listener?.onItemSelected(item)

            val intent = Intent(Constants.NOTIFICATION_NAME)
            intent.putExtra(Constants.DATA_KEY, item)
            val manager = LocalBroadcastManager.getInstance(context!!)
            manager.sendBroadcast(intent)

        }
    }
}