package practice.android.splitview

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.AttributeSet
import android.view.View

class DetailActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val fragment = fragmentManager.findFragmentById(R.id.detailFragment) as? DetailFragment
        fragment?.setDetailValue(intent.getStringExtra(DetailActivity.VALUE))
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {
        val VALUE = "textValue"
    }
}