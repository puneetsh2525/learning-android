package practice.android.splitview

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_list.*
import practice.android.splitview.RecyclerViewAdapters.ListRecyclerViewAdapter

class ListFragment:Fragment() {

    private var items:List<String> = listOf()
    private var adapter:ListRecyclerViewAdapter? = null
    private var listener:ListRecyclerViewAdapter.OnItemSelectionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(arguments != null) {
            items = arguments.getStringArrayList(ARG_LIST) ?: listOf()
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listView.layoutManager = LinearLayoutManager(this.activity)
        setItems(items)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    public fun setItems(items:List<String>) {
        this.items = items
        if(this.isAdded && items != null) {
            adapter = ListRecyclerViewAdapter(items)
            listView.adapter = adapter
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is ListRecyclerViewAdapter.OnItemSelectionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        val ARG_LIST = "items"
    }
}